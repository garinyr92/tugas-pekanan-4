package apitesting;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class ReqresTest {
  @BeforeClass
  public void setup(){
    RestAssured.baseURI = "https://reqres.in";
  }

  @Test
  public void getListUsers(){
    given().
    when().
            get("/api/users?page=2").
    then().
            statusCode(200).
            body(containsString("michael.lawson@reqres.in")).
    log().all();
  }

  @Test
  public void getSingleUser(){
    Map<String, String> data = new HashMap<>();
    data.put("email", "janet.weaver@reqres.in");
    data.put("first_name", "Janet");
    data.put("last_name", "Weaver");


    given().
    when().
            get("/api/users/2").
    then().
            statusCode(200).
            assertThat().
            body("data.email", equalTo(data.get("email"))).
            body("data.first_name", equalTo(data.get("first_name"))).
            body("data.last_name", equalTo(data.get("last_name"))).
    log().all();
  }

  @Test
  public void createUser(){
    String userName = "morheus";
    String userJob = "leader";

    Map<String, String> user = new HashMap<>();
    user.put("name", userName);
    user.put("job", userJob);

    given().
            contentType("application/json").
            body(user).
    when().
            post("api/users").
    then().
            statusCode(201).
            body("name", equalTo(userName)).
            body("job", equalTo(userJob)).
            body("id", notNullValue()).
            body("createdAt", notNullValue()).
    log().all();
  }

  @Test
  public void updateUser(){
    String userName = "morheus";
    String userJob = "zion resident";

    Map<String, String> user = new HashMap<>();
    user.put("name", userName);
    user.put("job", userJob);

    given().
            contentType("application/json").
            body(user).
    when().
            put("api/users/2").
    then().
            statusCode(200).
            body("name", equalTo(userName)).
            body("job", equalTo(userJob)).
            body("updatedAt", notNullValue()).
    log().all();
  }

  @Test
  public void deleteUser(){
    given().
    when().
            delete("/api/users/2").
    then().
            statusCode(204).
            body(emptyOrNullString()).
    log().all();
  }
}
